import React from 'react'
import { FlatList } from 'react-native'
import PropTypes from 'prop-types'

import Card from './Card'
/* getImageFromId*/

const keyExtractor = ({id}) => id.toString();

const getImageFromId = (id) => {
    return `https://picsum.photos/id/${id}/600/600`
}

export default class CardList extends React.Component{
    static propTypes = {
        items: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                author: PropTypes.string.isRequired
            })
        ).isRequired,
        commentsForItem: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string)).isRequired,
        onPressComments: PropTypes.func.isRequired,
    };

    renderItem = ({ item: { id, author } }) => {
        const { commentsForItem, onPressComments } = this.props;
        const comments = commentsForItem[id];
        
        return(
        <Card
            fullname={author}
            image={{
                uri: getImageFromId(id)
            }}
            linkText={`${comments ? comments.length : 0} Comments`}
            onPressLinkText={()=>onPressComments(id)}
        />)
    }

    render(){
        const { items, commentsForItem} = this.props;

        return(
            <FlatList
                data={items}
                renderItem={this.renderItem}
                keyExtractor={keyExtractor}
                extraData={commentsForItem}
            />
        )
    }
}