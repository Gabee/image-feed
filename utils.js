export function getInitials(fullname){
    return fullname.split(' ').map(name => {
        return name[0].charAt(0)
    }).join('');
}

export function getAvatarColor(fullname){
    return 'purple'
}

export async function fetchImages(){
    var objArray = [];

    await fetch("https://picsum.photos/v2/list")
        .then(response => response.json())
        .then(response => objArray = response);

    return objArray.map(author => ({id: parseInt(author.id), author: author.author}))
}