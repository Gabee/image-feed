import React from 'react'
import { Modal, View, StyleSheet, Platform } from 'react-native'
import Constants from 'expo-constants'
import PropTypes from 'prop-types'

import Feed from './screens/Feed'
import Comments from './screens/Comments'

const items = [
  {id: 0, author: "Luis Nani"},
  {id: 1, author: "Chris Mueller"}
]

export default class App extends React.Component{
  state = {
    commentsForItem: { 0: ["test", "data", "refer"]},
    showModal: false,
    selectedItemId: null,
  }

  openCommentScreen = id => {
    this.setState({
      showModal: true,
      selectedItemId: id,
    })
  }

  closeCommentScreen = () => {
    this.setState({
      showModal: false,
      selectedItemId: null,
    })
  }

  render(){
    const { commentsForItem, showModal, selectedItemId } = this.state;

    return(
      <View style={styles.container}>
        <Feed 
          style={styles.feed}
          commentsForItem={commentsForItem}
          onPressComments={this.openCommentScreen} 
        />
        <Modal
          visible={showModal}
          animationType="slide"
          onRequestClose={this.closeCommentScreen}
        >
          <Comments
            style={styles.container}
            comments={commentsForItem[selectedItemId] || []}
            onClose={this.closeCommentScreen}>
          </Comments>
        </Modal>
      </View> 
    )
  }
}

const platformVersion = Platform.OS === 'ios' ? parseInt(Platform.Version, 10) : Platform.Version;

const styles = StyleSheet.create({
  container:{
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: '#fff'
  },
  feed: {
    flex: 1,
    marginTop:
      Platform.OS === 'android' || platformVersion < 11
        ? Constants.statusBarHeight : 0
  },
  comments: {
    flex: 1,
    marginTop:
      Platform.OS === 'ios' && platformVersion < 11
        ? Constants.statusBarHeight : 0,
  }
})